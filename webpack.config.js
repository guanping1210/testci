const path = require('path');
const webpack = require('webpack');

// 自动生成html的插件-根据模板页面生成新的页面
const HtmlWebpackPlugin = require('html-webpack-plugin');
// 清除之前打包的文件
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    entry: {
        index: './app.jsx',
    },
    output: { 
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.jsx$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react', 'stage-0'],
                },
            },
        ],
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({ template: './index.html'}),
        new CleanWebpackPlugin(['dist']),
    ],
};